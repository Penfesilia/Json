﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;

namespace library
{
    class WorkWithXML
    {
        public static void SaveToXML()
        {
            XDocument xdoc = new XDocument(new XElement("library",
                 new XElement("department",
                        new XAttribute("titleOfDep", "Зарубiжна лiтература"),
                        new XElement("book",
                            new XAttribute("title", "Дiти капiтана Гранта"),
                            new XAttribute("author", "Жюль Верн"),
                            new XAttribute("volume", 564)),
                        new XElement("book",
                            new XAttribute("title", "За 80 днiв навколо свiту"),
                            new XAttribute("author", "Жюль Верн"),
                            new XAttribute("volume", 801)),
                        new XElement("book",
                            new XAttribute("title", "Один день"),
                            new XAttribute("author", "Девiд Нiколсон"),
                            new XAttribute("volume", 453)),
                        new XElement("book",
                            new XAttribute("title", "Тягар людських пристрастей"),
                            new XAttribute("author", "Сомерсет Моем"),
                            new XAttribute("volume", 611)),
                        new XElement("book",
                            new XAttribute("title", "Театр"),
                            new XAttribute("author", "Сомерсет Моем"),
                            new XAttribute("volume", 753)),
                        new XElement("book",
                            new XAttribute("title", "Вiзерунковий покрив"),
                            new XAttribute("author", "Сомерсет Моем"),
                            new XAttribute("volume", 796))),
                 new XElement("department",
                        new XAttribute("titleOfDep", "Українська лiтература"),
                        new XElement("book",
                            new XAttribute("title", "Чорна рада"),
                            new XAttribute("author", "Пантелеймон Кулiш"),
                            new XAttribute("volume", 489)),
                        new XElement("book",
                            new XAttribute("title", "Кобзар"),
                            new XAttribute("author", "Тарас Шевченко"),
                            new XAttribute("volume", 500)),
                        new XElement("book",
                            new XAttribute("title", "Земля"),
                            new XAttribute("author", "Ольга Кобилянська"),
                            new XAttribute("volume", 433)),
                        new XElement("book",
                            new XAttribute("title", "Тигролови"),
                            new XAttribute("author", "Iван Багряний"),
                            new XAttribute("volume", 390)),
                        new XElement("book",
                            new XAttribute("title", "В недiлю рано зiлля копала"),
                            new XAttribute("author", "Ольга Кобилянська"),
                            new XAttribute("volume", 447))),
                  new XElement("department",
                        new XAttribute("titleOfDep", "IT"),
                        new XElement("book",
                            new XAttribute("title", "Microsoft Visual C#"),                            
                            new XAttribute("author", "Джон Шарп"),
                            new XAttribute("volume", 848)),
                        new XElement("book",
                            new XAttribute("title", "Фiлософiя Java"),                           
                            new XAttribute("author", "Брюс Еккель"),
                            new XAttribute("volume", 1050)),
                        new XElement("book",
                            new XAttribute("title", "Изучаем Python"),                       
                            new XAttribute("author", "Марк Лутц"),
                            new XAttribute("volume", 948))),
                   new XElement("department",
                        new XAttribute("titleOfDep", "Психологiя"),
                        new XElement("book",
                            new XAttribute("title", "Мужчины с Марса, женщины с Венеры"),
                            new XAttribute("author", "Джон Грей"),
                            new XAttribute("volume", 548)),
                       new XElement("book",
                            new XAttribute("title", "Мистецтво любити"),                           
                            new XAttribute("author", "Ерiх Фромм"),
                            new XAttribute("volume", 835)),
                        new XElement("book",
                            new XAttribute("title", "7 звичок надзвичайно ефективних людей"),
                            new XAttribute("author", "Стiвен Ковi"),
                            new XAttribute("volume", 407)),
                        new XElement("book",
                            new XAttribute("title", "Как перестать беспокоиться и начать жить"),
                            new XAttribute("author", "Дейл Карнегi"),
                            new XAttribute("volume", 512)))));
            xdoc.Save("library.xml");
        }
        public static void AddToXML(Book b)
        {
            XDocument xdoc = XDocument.Load("library.xml");
            XPathNavigator navigator = xdoc.CreateNavigator();
            XElement book = new XElement("book",
                                new XAttribute("title", b.Title),
                                new XAttribute("author", b.NameOfAuthor),
                                new XAttribute("volume", b.Volume));

           xdoc.Element("library").Element("department").Element("book").AddBeforeSelf(book);
            xdoc.Save("library.xml");
        }
        public static void ReadFromXML()
        {
            XDocument xdoc = XDocument.Load("library.xml");
            foreach (XElement depElement in xdoc.Element("library").Elements("department"))
            {
                XAttribute titleAttribute = depElement.Attribute("titleOfDep");
                Console.WriteLine("Назва вiддiлу: {0}", titleAttribute.Value);
                foreach (XElement bookElement in depElement.Elements("book"))
                {
                    XAttribute title = bookElement.Attribute("title");
                    XAttribute author = bookElement.Attribute("author");
                    XAttribute volume = bookElement.Attribute("volume");
                    if ((title != null) && (author != null) && (volume != null))
                    {
                        Console.WriteLine("Назва книги: {0}", title.Value);
                        Console.WriteLine("Автор: {0}", author.Value);
                        Console.WriteLine("Обсяг: {0}", volume.Value);
                        Console.WriteLine();
                    }
                }

                  
                Console.WriteLine();
            }
        }
        public static void UpdateXML()
        {
            XDocument xdoc = XDocument.Load("library.xml");
            var items = from item in xdoc.Element("library").Elements("department")
                        where item.Attribute("titleOfDep").Value.Equals("IT")
                        select item;
            foreach (XElement itemElement in items.Elements("book"))
            {
                itemElement.SetAttributeValue("volume", Convert.ToInt32(itemElement.Attribute("volume").Value)+1);
            }
            xdoc.Save("library.xml");
        }
        public static void RemoveFromXML(Author a)
        {
            XDocument xdoc = XDocument.Load("library.xml");
            xdoc.Element("library").Elements("department").Elements("book").Where(b => b.Attribute("author").Value.Equals(a.NameOfAuthor)).Remove();
            xdoc.Save("library.xml");
        }
    }

}
