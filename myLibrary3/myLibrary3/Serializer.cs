﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.IO;

namespace library
{
    class Serializer
    {
        public static void SerializeToJson(List<Book> l)
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(List<Book>));
            using (FileStream fs = new FileStream("library.json", FileMode.OpenOrCreate))
            {
                jsonFormatter.WriteObject(fs, l);
            }

        }

        public static void DeserializeFromJson()
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(List<Book>));
            using (FileStream fs = new FileStream("library.json", FileMode.OpenOrCreate))
            {
                List<Book> newlibr = (List<Book>)jsonFormatter.ReadObject(fs);
                foreach (Book b in newlibr)
                {
                    Console.WriteLine("Назва: {0} \n Вiддiл: {1} \n  Автор: {2} \n  Обсяг: {3} \n",b.Title,  b.titleOfDep, b.NameOfAuthor,b.Volume);
                }
            }
        }
    }
}
