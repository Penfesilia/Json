﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace library
{
    class Author : IComparable, ICountingBooks
    {
        private string nameOfAuthor;
        public int countOfBooks;

        public string NameOfAuthor { get => nameOfAuthor; set => nameOfAuthor = value; }

        public Author(string name)
        {
            NameOfAuthor = name;
        }
        public int CompareTo(object otherAuthor)
        {
            return this.countOfBooks - ((Author)otherAuthor).countOfBooks;
        }
        public int count()
        {
            // використання LINQ
            var count = Library.Books.Count(book => NameOfAuthor.Equals(book.NameOfAuthor));
             return count;
        }
        public override string ToString()
        {
            return this.NameOfAuthor;
        }
        public static void MaxAuthor(List<Book> l)
        {
            List<Author> a = new List<Author>();
            foreach (Book b in l)
            {
                a.Add(new Author(b.NameOfAuthor));
                
            }
            foreach (Author author in a)
            {
                author.countOfBooks = author.count();
            }
            Author max = a[0];
            foreach (Author author in a)
            {
                if (author.CompareTo(max) > 0) max = author;
            }
            Console.WriteLine("В нашiй бiблiотецi набiльше книг такого автора: " + max.ToString());
        }

    }

}
