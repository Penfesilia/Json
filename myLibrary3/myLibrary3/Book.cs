﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;


namespace library
{
    [DataContract]
    class Book : IComparable
    {
        [DataMember]
        private string title;
        [DataMember]
        private string nameOfAuthor;
        public string Title { get => title; set => title = value; }

        public string NameOfAuthor
        {
            get => nameOfAuthor;
            set => nameOfAuthor = value;
        }

        public int Volume { get => volume; set => volume = value; }
       
        [DataMember]
        public string titleOfDep;
        [DataMember]
        private int volume;
        public Book(string title, string name, int vol)
        {
            this.Title = title;
            NameOfAuthor = name;
            Volume = vol;
        }
        public Book(string title, string dep, string name, int vol) :
            this(title, name, vol)
        {
            titleOfDep = dep;
        }
        public int CompareTo(object otherBook)
        {
            return this.Volume - ((Book)otherBook).Volume;
        }
        public override string ToString()
        {
            return this.Title;
        }
        public static void MinPage(List<Book> l)
        {
            Book min = l[0];
            foreach (Book b in l)
            {
                if (b.CompareTo(min) < 0) min = b;
            }
            Console.WriteLine("В нашiй бiблiотецi найменше сторiнок у книзi: " + min.ToString());
        }
    }

}
